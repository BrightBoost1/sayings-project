import { Injectable } from '@angular/core';
import { Saying } from '../models/saying.model';
@Injectable({
  providedIn: 'root',
})
export class SayingsService {
  // The data it manages
  private categories: Array<string>;
  private sayings: Array<Saying>;
  // The initialization of the data
  constructor() {
    // load the categories
    this.categories = ['Inspiration', 'Money', 'Staying Safe', 'Statistics'];
    // load the sayings
    this.sayings = [
      new Saying(
        'Inspiration',
        "You're braver than you believe, and stronger than you seem, and smarter than you think.",
        'piglet'
      ),
      new Saying(
        'Inspiration',
        'Nothing is particularly hard if you break it down into small jobs.',
        'Henry Ford'
      ),
      new Saying(
        'Staying Safe',
        'An apple a day keeps the doctor away.',
        'Chuck'
      ),
      new Saying(
        'Staying Safe',
        'A ship in a harbor is safe, but that is not what ships are for.',
        'Columbus'
      ),
      new Saying(
        'Statistics',
        'He uses statistics as a drunken man uses lamp posts... for support rather than illumination.',
        'Mark Twain'
      ),
    ];
  }
  // Methods callable from anywhere in the Angular app
  public getCategories(): Array<string> {
    return this.categories;
  }
  public getSayings(): Array<Saying> {
    return this.sayings;
  }
  public getSayingsThatMatchCategory(category: string): Array<Saying> {
    let matching = this.sayings.filter((s) => (s.category == category));
    return matching;
  }

  public getSayingsByPerson(person: string): Array<Saying> {
    let matching = this.sayings.filter((s) => s.person == person);
    return matching;
  }
}
