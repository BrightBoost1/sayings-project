import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SayingsService } from './providers/saying.service';
import { SayingsComponent } from './sayings/sayings.component';

@NgModule({
  declarations: [
    AppComponent,
    SayingsComponent
  ],
  imports: [
    BrowserModule, FormsModule
  ],
  providers: [SayingsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
