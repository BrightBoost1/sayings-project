export class Saying {
  constructor(
    public category: string,
    public quote: string,
    public person: string
  ) {}
}
