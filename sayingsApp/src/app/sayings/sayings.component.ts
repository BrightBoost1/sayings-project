import { Component } from '@angular/core';
import { Saying } from '../models/saying.model';
import { SayingsService } from '../providers/saying.service';
@Component({
  selector: 'app-sayings',
  templateUrl: './sayings.component.html',
  styleUrls: ['./sayings.component.css'],
})
export class SayingsComponent {
  title = 'sayings';
  person: string = '';
  // this is the data the HTML template binds to
  categories: Array<string> = [];
  // this is the matching sayings the HTML template binds to
  matchingSayings: Array<Saying> = [];
  // receive the SayingsService we need to make calls to
  constructor(private sayingsService: SayingsService) {}
  // when the component loads, get the categories
  ngOnInit() {
    this.categories = this.sayingsService.getCategories();
  }
  // when they pick from the dropdown, get the matching sayings
  onSelectSaying(event: any): void {
    const selectedCategory = event.target.value;
    // if they pick the select one option... show nothing
    if (selectedCategory == '') {
      this.matchingSayings = [];
    } else {
      // otherwise... find the matching sayings and show
      this.matchingSayings =
        this.sayingsService.getSayingsThatMatchCategory(selectedCategory);
    }
  }

  onClickGetSayingsByPerson() {
    this.matchingSayings = this.sayingsService.getSayingsByPerson(this.person);
  }
}
